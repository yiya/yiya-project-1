import bagel.Image;
import bagel.util.Point;

public abstract class Entity extends Image {
    private double x, y;

    protected Entity(String filename, double x, double y) {
        super(filename);
        this.x = x;
        this.y = y;
    }

    protected Point getPosition() {
        return new Point(this.x, this.y);
    }

    protected double getX() {
        return x;
    }

    protected void setX(double x) {
        this.x = x;
    }

    protected double getY() {
        return y;
    }

    protected void setY(double y) {
        this.y = y;
    }
}