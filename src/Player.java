import bagel.Window;

public class Player extends Entity{
    private Energy energy;

    protected Player(String filename, double x, double y, int energy) {
        super(filename, x, y);
        this.energy = new Energy(energy);
    }

    protected int getEnergy() {
        return energy.getEnergy();
    }

    protected void updateEnergy(int amount) {
        this.energy.add(amount);
    }

    protected void eat(Sandwich sandwich) {
        this.updateEnergy(+5);
        sandwich.eaten();
    }
}
